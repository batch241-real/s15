console.log("Hello World!");
// console.log() is useful for printing values of variables or certain results of code into the browser's console.

// Single Line Comment
/* Multiple Line Comment */

// Sytanx & Statements
/*
	- Statements in programming are instruction that we tell the computer to perform.
	- JS statements usually end with semicolon (;)
	- Syntax in programming, it is the set of rules that we describes statements must be constructed.
	- All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.
*/

// Variable
/*
	- Variables are used to contain data.
	- Any information that is used by an application is stored in what we call the memory.
	- When we create variables, certain portion of device's memory is given a name that we call variables.
	- This maes it easier for us to associate information stored in our devices to actual "names" information
*/

// Declaring Variables
/*
 	It tells our devices that a variable name is created and is ready to store data
 	Syntax
 		let/const variableName;

 */

 let myVariable = "Ada Lovelace";
 const constVariable = "John Doe"
 // const keyword is use when the value of the variable won't change.
 console.log(myVariable);

/*
	Guides in writing Variables
		1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
		2. Variable names should start with lowercase character, use camelCasing for multiple words.
		3. for constant variables, use the 'const keyword'
			note: if we use const keyword in declaring a variable, we cannot change the valueof its variable.
		4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion.
*/

// Declare and initialize
/*
	Initializing variables - the instance when a variable is given its first/inital value
	Syntax
		let/const variableName = initial
*/

let productName = "desktop computer"
let desktopName;
desktopName = "Dell"
console.log(desktopName)

/*
	Scope essentially means where these variables are available for use

	let/const are block scope

	A block is a chunk of code bounded by {}.
*/

/*
let outerVariable = "Hello";

{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}
console.log(outerVariable);
*/

/*
const outerVariable = "Hello";
{
	const innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);
*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);

// Multiple variable declaration

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// Data Types
/*
	- Strings are a series of characters that create a word, phrase, sentence, or anything related to creating a text.
	- Strings in JavaScript can be written using either a single ('') or double ("") quote.
*/
let country = "Philippines";
let province = 'Metro Manila'
console.log(country);
console.log(province);

// Concatenate strings
/* Multiple string values can be combines to create a single string using the "+" symbol */

let fullAddress = province +', '+ country;
console.log(fullAddress);

let greeting = 'I lve in the '+ country;
console.log(greeting);

let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

// "\n" refers to creating a new line in between text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Numbers
// Integers or Whole Number
let headCount = 27;
console.log(headCount);
console.log(typeof headCount);

// Decimal Number
let grade = 98.7;
console.log(grade);
console.log(typeof grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);
console.log(typeof planetDistance)

// String
let planetDistance1 = "28,000";
console.log(planetDistance1);
console.log(typeof planetDistance1);

// Combining text and strings
console.log("John's grade last quarter is"+ grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// true or false
let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// Arrays
/*
	Arrays are special kind data type that's use to store multiple values
	Syntax:
	let/cost arrayName = [elementA, elementB ...];
*/

// Similar data types
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);
// Array is a special kind of object
console.log(typeof grades);

// Different data types
let details = ["John", 32, true];
console.log(details);

// Objects
/*
	Objects are another special kind of data type that's used to mimic real world objects/items
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
*/

let person = {
	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["+63123456789", "8123-4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thirdGrading: 90.1,
	fourthGrading: 94.7
}

/*
	Constant Objects and Arrays
	We can change the element of an array to a constant variable
*/

const anime = ["One Piece", "One Punch Man", "Your Lie in April"];
console.log(anime);
// arrayName[indexNumber]
anime[0] = ['Kimetsu no Yaiba'];
console.log(anime);

const anime1 = ["One Piece", "One Punch Man", "Your Lie in April"];
console.log(anime1);
/* anime1 = ['Kimetsu no Yaiba'];
console.log(anime1); */

// Null
// It is used to intentionally express the absence of a value a variable declaration/initialization

let spouse = null;
console.log(spouse);

let myNumber = 0;
let myString = "";

// Undefined
// Represent the state of the variable that has been declared but without an assigned value

let fullName;
console.log(fullName);